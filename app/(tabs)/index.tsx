import { StyleSheet, Image } from 'react-native';

import EditScreenInfo from '@/components/EditScreenInfo';
import { Text, View } from '@/components/Themed';

export default function TabOneScreen() {
  return (
    <View style={styles.container}>
      <Image
          source={require('../../assets/images/garden.png')}
          style={styles.logo}
      />
      <Text style={styles.title}>Garden</Text>
      <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
      <EditScreenInfo path="app/(tabs)/index.tsx" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    width: 80,
    height: 80,
    resizeMode: 'contain', // Esto mantiene las proporciones de la imagen
  },
  title: {
    fontSize: 40,
    fontWeight: 'bold',
    color:'#ffbd4a'
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
});
