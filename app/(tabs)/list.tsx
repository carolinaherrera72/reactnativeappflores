import React, {useState} from 'react';
import {ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View} from 'react-native';

function AddFlowers() {
    const [task, setTask] = useState('');
    const [tasks, setTasks] = useState([]);

    const handleAddTask = () => {
        const newTask = { title: task, completed: false, createdAt: new Date().toISOString() };
        const updatedTasks = [...tasks, newTask];
        // @ts-ignore
        setTasks(updatedTasks);
        setTask(''); // Reset input after submission
    };

    const handleClearTasks = () => {
        setTasks([]);
    };

    // @ts-ignore
    return (
        <View style={styles.container}>
            <Text
                style={styles.getStartedText}
                >
                Agrega las flores que desees:
            </Text>
            <TextInput
                placeholder="Agregar tus deseos"
                value={task}
                onChangeText={setTask}
                style={styles.input}
            />
            <View style={styles.buttonContainer}>
            <TouchableOpacity onPress={handleAddTask} style={styles.button}>
                <Text style={styles.buttonText}>Agregar</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={handleClearTasks} style={styles.clear}>
                <Text style={styles.buttonText}>Limpiar</Text>
            </TouchableOpacity>
            </View>
            <ScrollView style={styles.list}>
                {tasks.map((item, index) => {
                    const {title} = item;
                    return (
                        <View key={index} style={styles.itemContainer}>
                            <Text style={styles.item}>{index + 1}. {title}</Text>
                        </View>
                    );
                })}
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginHorizontal: 10,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 50,
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        width: '100%',
        paddingHorizontal: 10, // Añadir espacio horizontal
    },
    getStartedText:{
        color:'#fff',
        padding: 10,
        marginVertical: 5,
    },
    button: {
        backgroundColor: '#dccfb9',
        padding: 10,
        borderRadius: 5,
        marginVertical: 20,
    },
    clear: {
        backgroundColor: '#fff',
        padding: 10,
        borderRadius: 5,
        marginVertical: 20,
    },
    buttonText: {
        color: '#000',
        fontSize: 16
    },
    input: {
        borderBottomWidth: 1,
        borderBottomColor: '#ffbd4a',
        marginRight: 10,
        height: 50,
        padding: 5,
        width: 200,
        backgroundColor: '#fff',
        borderRadius: 10,
        color: '#000',
        marginVertical: 10,
        shadowColor: '#000',
    },
    itemContainer: {
        padding: 10,
        marginVertical: 5,
        borderRadius: 10,
        alignItems: 'flex-start',
        borderBottomColor:'#fff',
        borderStyle:"solid",
        borderBottomWidth:1
    },
    item: {
        color: 'white',

    },
    list: {
        flex: 3,
        width: '100%'
    }
});

export default AddFlowers;
